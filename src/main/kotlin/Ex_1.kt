import java.util.Scanner
/*
Exercici una llista de la llargaria donada per l'usuari plena de nombres aleatoris entre l'1 i el 300
els mostra i després demana un digit i mostra els nombresd de la llita acabats per aquell digit.
Entrada: Ints, preguntes a l'usuari
Sortida: Ints
 */
/**
 * @author POL MARTÍNEZ RODRIGUEZ
 */
fun main () {
    val scanner = Scanner(System.`in`)
    println("Introdueix la llargada de la seqüéncia:")
    val sizeSequence = scanner.nextInt()
    val sequence : MutableList<Int> = MutableList(sizeSequence) { (1..300).random() } //creador de seqüéncia
    for ( i in sequence) print("$i ")
    println()
    println("Introdueix un digit:")
    val digit = scanner.nextInt()
    val sequenceByTheDigit: MutableList<Int> = mutableListOf()
    println("Numeros que acaben per $digit:")
    for (i in sequence.indices){
        if (sequence[i] % 10 == digit) sequenceByTheDigit.add(sequence[i])
    }
    for (i in sequenceByTheDigit) print("$i ")
}