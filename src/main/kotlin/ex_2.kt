import java.util.*
/*
Donada una seqüencia de paraules que acaba en fi, he de trobar quina es la llargada més alta de la seqüencia
composada per la primera paraula introduïda.
entrada: Strings, (seq. Strings)
sortida: Int (llargada de la seqüencia)
 */
/**
 * @author POL MARTÍNEZ RODRIGUEZ
 */
fun main () {
    val scanner = Scanner(System.`in`)
    val sequenceOfWords : MutableList<MutableList<String>> = mutableListOf()
    var iterator = 0
    println("Introdueix la seqüencia de paraules:")
    var word = scanner.next()
    sequenceOfWords.add(mutableListOf())
    sequenceOfWords[iterator].add(word)
    var max = 0
    while (word!="fi"){
        word = scanner.next()
        if (sequenceOfWords[0][0] == word) {
            sequenceOfWords[iterator].add(word)
        }
        else if (sequenceOfWords[0][0] != word && sequenceOfWords[iterator].isNotEmpty() && word != "fi") {
            sequenceOfWords.add(mutableListOf())
            if (sequenceOfWords[iterator].size > max) max = sequenceOfWords[iterator].size
            iterator ++
        }
        else if (sequenceOfWords[iterator].size > max) max = sequenceOfWords[iterator].size
    }
    println(sequenceOfWords)
    println("La llargada de la subseqüencia més llarga amb la paraula ${sequenceOfWords[0][0]} és : $max")
}