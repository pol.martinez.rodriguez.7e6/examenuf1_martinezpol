import java.util.*
/*
Demana nombre de files i columnes i construeix una matriu d'aquesta mesura, s'omple de manera manual introduint valors,
es mostra la matriu i si és quadrada es mutlipliquen els valors per sota la diagonal principal
Entrada: 2 Ints (Quantitat de columnes i files) + diversos Ints depenent de la quantitat de files i columnes.
Sortida: La matriu en forma de matriu, en cas que la matriu sigui quadrada també la
mutliplicació de tots els nombres per sota la diagonal principal.
 */
/**
 * @author POL MARTÍNEZ RODRIGUEZ
 */
fun main () {
    val scanner = Scanner(System.`in`)
    val matriz : MutableList<MutableList<Int>> = mutableListOf()
    println("Introdueix la quantitat de files:")
    val rows = scanner.nextInt()
    println("Introdueix la quantitat de columnes:")
    val cols = scanner.nextInt()
    println("Introdueix els valors: ")
    for ( i in 0 until  rows){
        matriz.add(mutableListOf())
        for (j in 0 until cols){
            matriz[i].add(scanner.nextInt())
        }
    }
    println("Aquesta és la teva matriu: ")
    for (i in 0 until rows){
        for (j in 0 until cols ){
            print("${matriz[i][j]} ")
        }
        println()
    }
    if (rows == cols){
        println("\nAquest és el resultat de multiplicar els nombres per sota de la diagonal principal:")
        var result = 1L
        for (i in 0 until cols){
            for (j in 0 until rows){
                if (j != i && j < i){
                    result*= matriz[i][j]
                }
            }
        }
        println(result)
    }
}