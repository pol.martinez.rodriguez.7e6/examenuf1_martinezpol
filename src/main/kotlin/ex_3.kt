import java.util.*
/*
un programa que vagi llegint strings per teclat per cada linea, i digui la frase amb més e i quantes té, fins que s'introdueixi la paraula fi
Entrada: String (L'agafo com una linea sencera)
Sortida: La frase amb més 'e' i la quantitat de 'e' d'aquesta.
 */
/**
 * @author POL MARTÍNEZ RODRIGUEZ
 */
fun main () {
    val scanner = Scanner(System.`in`)
    val listOfPhrases : MutableList<String> = mutableListOf()
    var counter : Int
    var max = 0
    var biggerPhrasePossition = 0
    do {
        println("Escriu una frase")
        val phrase = scanner.nextLine()
        listOfPhrases.add(phrase)
        counter = 0
        for (i in phrase){
            if (i.lowercaseChar() == 'e') counter +=1
        }
        if (counter > max) {
            max = counter
            biggerPhrasePossition = listOfPhrases.indexOf(phrase)
        }
        println("La frase amb més 'e' és : \"${listOfPhrases[biggerPhrasePossition]}\"\nTé $max 'e'.")
    }while (phrase != "fi")

}